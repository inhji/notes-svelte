import * as linkify from 'linkifyjs'
import 'linkify-plugin-hashtag'

export function createNote(noteTitle) {
	const hashtags = linkify.find(noteTitle, 'hashtag')
  const tags = hashtags.map(entry => entry.value.slice(1))
  const title = hashtags.reduce(removeHashtag, noteTitle)

  if (tags.length === 0) {
    tags.push("none")
  }

  return {
    title, 
    tags, 
    done: false,
    created: Date.now()
  }
}

export function applyFilter(notes, filter) {
  if (!filter || filter.length === 0) return notes
  return notes.filter(note => note.tags.includes(filter)) 
}

export function applySort(notes) {
  const sorted = notes
  sorted.sort((a, b) => {
    return b.created - a.created
  })
  sorted.sort((a, b) => {
    if (a.done && !b.done) return 1
    if (!a.done && b.done) return -1
    return 0
  })
  return sorted
}

export function applyFilterSort(notes, filter) {
  return applySort(applyFilter(notes, filter))
}

export function toggleDoneState(notes, id) {
	return notes.map(note => {
    if (note.created === id) {
      const done = note.done ? null : Date.now()

      // return a new object
      return {
        ...note,
        done
      };
    }

    // return the same object
    return note;
  });
}

function removeHashtag(newTitle, hashtag) {
	return newTitle.replace(hashtag.value, '').trim();
}
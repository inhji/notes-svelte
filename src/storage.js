export default class Storage {
	constructor() {
		this.prefix = "notesapp"
	}

	getFilter = () => this.#load('filter')
	saveFilter = (filter) => this.#save('filter', filter)

	getTheme = () => this.#load('theme')
	saveTheme = (theme) => this.#save('theme', theme)

	getNotes() {
		const str = this.#load('notes', [])
		try {
			return JSON.parse(str)
		} catch(e) {
			return []
		}
	}
	saveNotes = (notes) => this.#save('notes', JSON.stringify(notes))

	#save(key, value) {
		console.log(`saving key '${key}' with value '${value}'`)
		localStorage.setItem(`${this.prefix}-${key}`, value)
	}

	#load(key, defaultValue = '') {
		const value = localStorage.getItem(`${this.prefix}-${key}`) || defaultValue
		console.log(`loading key '${key}' with value '${value}'`)
		return value
	}
}
const opts = {
	setMinDays: 7,
	days: 'd',
	hours: 'h',
	minutes: 'm'
}

export default function (time) {
	return fuzzyTime(time, opts)
}

function fuzzyTime(datetime, options) {

    options = options || {};

    const space = ''
    const days = options.days || 'days';
    const hours = options.hours || 'hours';
    const minutes = options.minutes || 'minutes ago';
    const on = options.on || 'on';
    const ago = datetime
    const setMonthArray = options.setMonthArray || ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']
    const setMinDays = options.setMinDays || 7;
    const dateFormat = options.dateFormat || {};
    
    // later record end time
    var endTime = new Date();
    var agoUTC = new Date(datetime)

    // time difference in ms
    var timeDiff = endTime - agoUTC;
    // strip the miliseconds
    timeDiff /= 1000;

    // get seconds
    var seconds = Math.round(timeDiff % 60);

    // remove seconds from the date
    timeDiff = Math.floor(timeDiff / 60);

    // get minutes
    var getMinutes = Math.round(timeDiff % 60);

    // remove minutes from the date
    timeDiff = Math.floor(timeDiff / 60);

    // get hours
    var getHours = Math.round(timeDiff % 24);

    // remove hours from the date
    timeDiff = Math.floor(timeDiff / 24);

    // the rest of timeDiff is number of days
    var getDays = timeDiff;

    const formatDate = function() {
        var d = ago
        var mArr = setMonthArray
        var date = new Date(d)
        var dd = date.getDate(); 
        var mm = date.getMonth();
        var yyyy = date.getFullYear(); 
        if (dateFormat == 'simple'){
            return d = mArr[mm] + ' ' + dd + '\' ' + (yyyy).toString().slice(2)
        } else {
            return d = dd +' '+ mArr[mm] +' '+ yyyy
        }
    }

    var output;

    if (getDays != 0){
        if(getDays < setMinDays){
        		output = getDays + space + days
            // if (getHours != 0){
            //     output = getDays + space + days + space + getHours + space + hours //+ space + getMinutes + space + minutes
            // } else {
            //     output = getDays + space + days //+ space + getMinutes + space + minutes
            // }
        } else {
            output = on + space + formatDate()
        }

    } else {
        if (getHours != 0){
            output = getHours + space + hours //+ space + getMinutes + space + minutes
        } else {
            output = getMinutes + space + minutes
        }
    }

    return output
}

